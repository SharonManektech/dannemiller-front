/* Navbar toggler js for remove class "Show" from child */
$(document).on('ready', function () {

  $(".search-main").click(function () {
    $(".search-icon").toggleClass("hide");
    $(".close-icon").toggleClass("show");
    $(".search").toggleClass("active");
    $(".desktop-menu").toggleClass("hide");
    $(".s-box").toggleClass("show");

  });

  $(".filter").click(function () {
    $(".filter").toggleClass("hide");
    $(".close-btn").toggleClass("show");
    $(".filter-menu").toggleClass("hide");
    $(".dash-main").toggleClass("active");
  });



  $('.navBack').on('click', function (e) {
    $(this).parent().removeClass('show');
    e.stopPropagation();
  })



  $(".dash-title").click(function () {
    $(".dash-title").toggleClass("active");
    $(".account-info").slideToggle();
  });

  $(".select-wrap, .select-box").click(function () {
    $(this).toggleClass("open");
  });

  // $(".news-main").slice(0, 4).show();
  // $("#loadMore").on("click", function(e){
  //   e.preventDefault();
  //   $(".news-main:hidden").slice(0, 4).slideDown();
  //   if($(".news-main:hidden").length == 0) {
  //     $("#loadMore").text("No Content").addClass("noContent");
  //   }
  // });


});
$(document).ready(function () {
  $(".news-main").hide();
  $(".news-main").slice(0, 6).show();

  if ($(".news-main:hidden").length != 0) {
    $("#loadMore").show();
  }
  $("#loadMore").on('click', function (e) {
    e.preventDefault();
    $(".news-main:hidden").slice(0, 4).slideDown();
    if ($(".news-main:hidden").length == 0) {
      $("#loadMore").fadeOut('slow');
      $("#explore-buttonbox").fadeOut('slow');
    }
  });
});

jQuery(window).on('load', function () {
  $(".news-main").hide();
  $(".news-main").slice(0, 6).show();
});
/*hero slider js*/
var swiper = new Swiper('.hero-container', {
  slidesPerView: 1,
  loop: true,
  effect: 'fade',
  autoplay:
  {
    delay: 7000,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  autoplayDisableOnInteraction: false,
});
/*hero slider js end*/

// learn topic slider js
var swiper = new Swiper('.learn-container', {
  slidesPerView: 4,
  loop: true,
  speed: 1500,
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  autoplayDisableOnInteraction: false,
  breakpoints: {
    300: {
      slidesPerView: 1,
    },
    575: {
      slidesPerView: 2,
    },
    767: {
      slidesPerView: 3,
    },
    992: {
      slidesPerView: 4,
    },
  }
});
// learn topic slider js end

// event header js start
var swiper = new Swiper('.event-container', {
  slidesPerView: 2,
  spaceBetween: 30,
  speed: 1500,
  loop: true,
  autoplay:
  {
    delay: 4000,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  autoplayDisableOnInteraction: false,
  breakpoints: {
    300: {
      slidesPerView: 1,
    },
    991: {
      slidesPerView: 2,
    },
  }
});
// event header js end

// testimonial slider js
var swiper = new Swiper('.testimonial-container', {
  loop: true,
  speed: 2000,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
// testimonial slider js end

//company-slider
var swiper = new Swiper('.company-slider', {
  slidesPerView: 3.9,
  spaceBetween: 0,
  speed: 1500,
  loop: true,
  autoplay:
  {
    delay: 2000,
    disableOnInteraction: false,
  },
});

//User header
if ($(".dropdown").hasClass("show")) {
  $(this).parent().addClass('Added');
} else {
  $('.sign-details').removeClass('Added');
}


$('.anchor-link').click(function (e) {
  e.preventDefault();
  var target = $($(this).attr('href'));
  if (target.length) {
    var scrollTo = target.offset().top - 100;
    $('body, html').animate({ scrollTop: scrollTo + 'px' }, 800);
  }
});