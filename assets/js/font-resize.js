// Font Increase/Decrease and Reset

var isMobile = window.screen.width <= 1363;

var fontLevel = 0;

function resizableElements() {
    return document.querySelectorAll('.activity-points-wrap p, .activity-points-wrap span');
}

function downgradeFontSize() {
    if (fontLevel < 0) {
        return;
    }
    resizableElements().forEach(function (el) {
        resizeText(el, "substract", 2);
    })
    fontLevel--;
}

function upgradeFontSize() {
    if (fontLevel > 0) {
        return;
    }
    resizableElements().forEach(function (el) {
        resizeText(el, "add", 2);
    })
    fontLevel++;
}

function normalFontSize() {
    resizableElements().forEach(function (el) {
        resizeText(el, "normal", isMobile ? 14 : 16);
    })

    fontLevel = 0
}

resizeText = function (el, method, pixel) {
    var elNewFontSize, newSize;
    elNewFontSize = window.getComputedStyle(el);

    if (method == "add") {
        newSize = parseInt(elNewFontSize.fontSize.slice(0, -2)) + pixel + 'px';
    }
    if (method == "substract") {
        newSize = parseInt(elNewFontSize.fontSize.slice(0, -2)) - pixel + 'px';
    }
    if (method == "normal") {
        newSize = pixel + 'px';
    }
    console.log(newSize);
    el.style.fontSize = newSize;
    return elNewFontSize;
} 